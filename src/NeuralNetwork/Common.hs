{- |
    common types and functions
-}
module NeuralNetwork.Common (
    Input,
    Output,
    Target,
    Cost,
    LearningRate,
    OutputVector,
    TargetVector,
    CostVector,
    InputVector,
    transpose,
    sigmoid,
    train
) where
import qualified Data.Vector as V

-- |the cost type, also called error, to train a network means to minimize
--  this value
type Cost = Double

-- |the input
type Input = Double

-- |the output
type Output = Double

-- |the expected output
type Target = Double

-- |the learning rate, the bigger this is, the quicker a network is trained
--  however, bigger didn't mean better
type LearningRate = Double

-- | input vector is a list of input
type InputVector = V.Vector Double

-- |Output vector is a list of Output
type OutputVector = V.Vector Output

-- |Target vector is a list of Target
type TargetVector = V.Vector Target

-- |Cost vector is a list of Cost
type CostVector = V.Vector Cost

-- |transpose a metrix
transpose :: V.Vector (V.Vector a) -> V.Vector (V.Vector a)
transpose v = V.generate len (\n -> V.map (V.! n) v)
    where
    len = V.length $ V.head v

-- |sigmoid function
sigmoid :: Double -> Double
sigmoid z = 1 / (1 + e ** (-z))
    where
    e = exp 1

-- |general train function
--  the length of input and target must be the same
train ::
    Int ->
    LearningRate ->
    V.Vector InputVector ->
    V.Vector target ->
    (LearningRate -> InputVector -> target -> trainee -> trainee) ->
    trainee ->
    trainee
train count l input target trainF trainee = f count trainee
    where
    len = V.length input
    f 0 v = v
    f c v = f (c-1) $ trainF l i t v
        where
        i = input V.! (c `mod` len)
        t = target V.! (c `mod` len)
