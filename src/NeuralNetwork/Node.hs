{- |
    the Node datatype in a neural network

    this module is meant to be imported qualified
-}
module NeuralNetwork.Node (
    Node(..),
    generate,
    output,
    train,
    cost,
    costPrime,
    costWithCost,
    update
) where
import qualified Data.Vector as V
import Data.Random.Normal (normalIO)
import NeuralNetwork.Common hiding (train)

type Weight = Double

type Bias = Double

-- |the Node data type, contains weight for each input and a bias
data Node = Node (V.Vector Weight) Bias deriving (Show, Read)

-- |given how many input, generate a randomized node
generate :: Int -> IO Node
generate i = do
    weight <- V.replicateM i normalIO
    bias <- normalIO
    return $ Node weight bias

-- |given input and node, return the value this node outputs
output :: InputVector -> Node -> Output
output input (Node w b) = sigmoid $ (V.sum $ V.zipWith (*) input w) + b

-- |given output and targetOutput, return the cost value, this value
--  denotes how good this node is doing, the lower the better
cost :: Output -> Target -> Cost
cost o t = (o - t) ^ (2 :: Int)

-- |given output and targetOutput, return the cost value, this value
--  is given back to the update function for training
costPrime :: Output -> Target -> Cost
costPrime o t = o * (1 - o) * (t - o)

-- |for backward propagation
costWithCost :: InputVector -> Cost -> Node -> CostVector
costWithCost input c (Node weight _) = V.zipWith f input weight
    where
    f i w = i * (1 - i) * c * w

-- |given learning rate, input, cost and node, return a trained node
update :: LearningRate -> InputVector -> Cost -> Node -> Node
update l i c (Node w b) = Node (V.zipWith delta w i) (b + l * c)
    where
    delta weight input = weight + l * c * input

-- |given learning rate, input, targetOutput and node, return a trained node
train :: LearningRate -> InputVector -> Target -> Node -> Node
train l i t n = update l i (costPrime o t) n
    where
    o = output i n
