{- |
    the neural network

    this module is meant to be imported qualified
-}
module NeuralNetwork.Network (
    Network(..),
    generate,
    output,
    cost,
    train
) where
import Control.Monad
import NeuralNetwork.Common hiding (train)
import qualified NeuralNetwork.Layer as L

-- |the network data type, it consists of several layers
data Network = Network [L.Layer] deriving (Show, Read)

-- |given number of nodes of each layer, generate a randomized network
generate :: [Int] -> IO Network
generate [] = undefined
generate nodes@(_:ns) = Network <$> zipWithM L.generate nodes ns

-- |given input and network, return the value this network outputs
output :: InputVector -> Network -> OutputVector
output input (Network l) = foldl L.output input l

-- |given output and targetOutput, return the cost value, this value
--  denotes how good this node is doing, the lower the better
cost :: OutputVector -> TargetVector -> Cost
cost = L.cost

-- |given learning rate, input, targetOutput and network, return a trained one
train ::
    LearningRate -> InputVector -> TargetVector -> Network -> Network
train learn input target (Network layers) =
    Network $ reverse $ allL (reverse layers) (reverse $ allInputs input layers)
    where
    allL (l:ls) (o:i:os) =
        L.update learn i costOut l : hiddenL costBack ls os
        where
        costOut = L.costPrime o target
        costBack = L.costWithCost i costOut l
    allL [] [] = []
    allL _ _ = undefined
    hiddenL costOut (l:ls) (i:os) =
        L.update learn i costOut l : hiddenL costBack ls os
        where
        costBack = L.costWithCost i costOut l
    hiddenL _ [] [] = []
    hiddenL _ _ _ = undefined
    allInputs i (l:ls) = i : allInputs (L.output i l) ls
    allInputs i [] = i : []
