{- |
    the Layer datatype in a neural network

    this module is meant to be imported qualified
-}
module NeuralNetwork.Layer (
    Layer(..),
    generate,
    output,
    update,
    train,
    cost,
    costPrime,
    costWithCost
) where
import qualified Data.Vector as V
import NeuralNetwork.Common hiding (train)
import qualified NeuralNetwork.Node as N

-- |a Layer consists of many nodes
data Layer = Layer (V.Vector N.Node) deriving (Show, Read)

-- |given how many input and output, generate a randomized layer
generate :: Int -> Int -> IO Layer
generate inputNum nodeNum = Layer <$> V.replicateM nodeNum (N.generate inputNum)

-- |given input and the layer, calculate output
output :: InputVector -> Layer -> OutputVector
output input (Layer n) = V.map (N.output input) n

-- |given output and targetOutput, return the cost value, this value
--  denotes how good this node is doing, the lower the better
cost :: OutputVector -> TargetVector -> Cost
cost o t = V.sum $ V.zipWith N.cost o t

-- |given output and targetOutput, return the cost value, this value
--  is given back to the update function for training
costPrime :: OutputVector -> TargetVector -> CostVector
costPrime o t = V.zipWith N.costPrime o t

-- |for backward propagation
costWithCost :: InputVector -> CostVector -> Layer -> CostVector
costWithCost i c (Layer n) =
    V.map sum $ transpose $ V.zipWith (N.costWithCost i) c n

-- |given learning rate, input, cost and layer, return a trained layer
update :: LearningRate -> InputVector -> CostVector -> Layer -> Layer
update l i c (Layer n) = Layer $ V.zipWith (N.update l i) c n

-- |given learning rate, input, targetOutput and layer, return a trained layer
train :: LearningRate -> InputVector -> TargetVector -> Layer -> Layer
train l i t (Layer n) = Layer $ V.zipWith (N.train l i) t n
