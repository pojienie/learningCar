{- |
    this provides Parser data type that let's you parse string
-}
module Data.Parser (
    Parser(..),
    match,
    alphabet,
    number,
    alphanumeric,
    space,
    success,
    newline,
    eof,
    zeroOrMore,
    oneOrMore,
) where
import Control.Applicative

-- |this data type lets you construct a parser
data Parser a = Parser {runParser :: String -> Maybe (a, String)}

instance Functor Parser where
    fmap f (Parser run) = Parser $ \s ->
        case run s of
            Just (v, r) -> Just (f v, r)
            Nothing -> Nothing

instance Applicative Parser where
    pure a = Parser $ \s -> Just (a, s)
    (Parser run1) <*> parser2 = Parser $ \s ->
        case run1 s of
            Just (f, r) -> runParser (fmap f parser2) r
            Nothing -> Nothing

instance Monad Parser where
    return a = Parser $ \s -> Just (a, s)
    (Parser v) >>= f = Parser $ \s ->
        case v s of
            Just (v', r) -> runParser (f v') r
            Nothing -> Nothing

instance Alternative Parser where
    empty = Parser $ \_ -> Nothing
    (Parser a) <|> (Parser b) = Parser $ \s -> a s <|> b s

-- |match a character
match :: (Char -> Bool) -> Parser Char
match p = Parser f
    where
    f [] = Nothing
    f (x:xs)
        | p x       = Just (x, xs)
        | otherwise = Nothing

-- |match alphabet
alphabet :: Parser Char
alphabet = match (\c -> (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))

-- |match a number
number :: Parser Char
number = match (\c -> c >= '0' && c <= '9')

-- |match a number or alphabet
alphanumeric :: Parser Char
alphanumeric = alphabet <|> number

-- |match space or tab
space :: Parser Char
space = match ((||) <$> (== ' ') <*> (=='\t'))

-- |Parser a will success no matter what
success :: Parser a -> Parser ()
success p = (p >> pure ()) <|> pure ()

-- |match a new line
newline :: Parser ()
newline = match (== '\n') >> return ()

-- |check if it's the end of file
eof :: Parser Bool
eof = Parser f
    where
    f [] = Just (True, [])
    f s = Just (False, s)

-- |match zero or more Parser
zeroOrMore :: Parser a -> Parser [a]
zeroOrMore p = oneOrMore p <|> return []

-- |match one or more Parser
oneOrMore :: Parser a -> Parser [a]
oneOrMore p = (:) <$> p <*> zeroOrMore p
