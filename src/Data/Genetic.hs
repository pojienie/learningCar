{- |
    this module implements genetic algorithm
-}
module Data.Genetic
    ( CanReproduce
    , reproduce
    , mutate
    ) where

import Data.Random.Normal (normalIO)
import System.Random (randomRIO)
import qualified Data.Vector as V
import qualified NeuralNetwork.Node as N
import qualified NeuralNetwork.Layer as L
import qualified NeuralNetwork.Network as NN

-- | monadic if
ifM :: (Monad m) => m Bool -> m a -> m a -> m a
ifM p a b = p >>= (\p' -> if p' then a else b)

-- | the chances of something happening
chance ::  Float -> IO a -> IO a -> IO a
chance f a b = ifM ((< f) <$> randomRIO (0 :: Float, 1)) a b

-- | monadic zipWith
zipWithM :: (Monad m) => (a -> b -> m c) -> [a] -> [b] -> m [c]
zipWithM f a b = sequence $ zipWith f a b

-- | data that has this class instance can reproduce a child
class CanReproduce a where
    -- | given a father and a mother, randomly combine them to produce a child
    reproduce :: a -> a -> IO a
    reproduce a b = chance 0.5 (pure a) (pure b)
    -- | given mutation chance and a body, return a mutated body
    mutate :: Float -> a -> IO a

instance CanReproduce Float where
    mutate c v = normalIO >>= (\r -> chance c (pure $ v + r) (pure v))

instance CanReproduce Double where
    mutate c v = normalIO >>= (\r -> chance c (pure $ v + r) (pure v))

instance (CanReproduce a) => CanReproduce (V.Vector a) where
    reproduce = V.zipWithM reproduce
    mutate f = V.mapM (mutate f)

instance CanReproduce N.Node where
    reproduce (N.Node v1 b1) (N.Node v2 b2)
        = N.Node <$> reproduce v1 v2 <*> reproduce b1 b2
    mutate f (N.Node v b) = N.Node <$> mutate f v <*> mutate f b

instance CanReproduce L.Layer where
    reproduce (L.Layer v1) (L.Layer v2) = L.Layer <$> reproduce v1 v2
    mutate f (L.Layer v) = L.Layer <$> mutate f v

instance CanReproduce NN.Network where
    reproduce (NN.Network l1) (NN.Network l2)
        = NN.Network <$> zipWithM reproduce l1 l2
    mutate f (NN.Network l) = NN.Network <$> mapM (mutate f) l
