{- |
    This module defines `Point` datatype
-}
module LearningCar.Data.Point
    ( Point(..)
    , (.+.)
    , distance
    ) where

import Data.Default (Default, def)
import LearningCar.Data.CanDraw (CanDraw, draw)
import qualified Graphics.Gloss as G

-- | a point, the same as Point in Gloss, but wrapped in new Point
newtype Point = Point
    {
      -- | get inner representation of Point
        getTuple :: (Float, Float)
    } deriving (Show, Read)

instance Default Point where
    def = Point (0, 0)

instance CanDraw Point where
    draw (Point (x, y)) = pure $ G.Translate x y $ G.circleSolid 1

-- | Point add Point
infixl 6 .+.
(.+.) :: Point -> Point -> Point
(.+.) (Point (x1, y1)) (Point (x2, y2)) = Point (x1 + x2, y1 + y2)

-- | calcuate the distance of two points
distance :: Point -> Point -> Float
distance (Point (x1, y1)) (Point (x2, y2)) = sqrt $ x^two + y^two
    where
    -- without this, I get default type warning.
    two :: Int
    two = 2
    x = abs $ x1 - x2
    y = abs $ y1 - y2
