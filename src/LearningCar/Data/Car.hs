{-# LANGUAGE FlexibleContexts #-}
{- |
    Deep learning car library
-}
module LearningCar.Data.Car
    ( Status(..)
    , Car(..)
    , initCar
    , start
    , step
    , getFourPoints
    , kill
    , getSensorPoints
    ) where

import Data.Default (def)
import Data.Maybe (fromJust, isJust)
import Data.List (minimumBy)
import Data.Function (on)
import Data.Genetic (CanReproduce, reproduce, mutate)
import LearningCar.Data.CanDraw (CanDraw, draw)
import LearningCar.Data.Point (Point(..), (.+.), distance)
import Graphics.Gloss.Geometry.Angle (radToDeg, normalizeAngle, degToRad)
import Graphics.Gloss.Geometry.Line (intersectLineLine)
import Control.Monad.Reader (MonadReader(..), MonadIO(..))
import qualified LearningCar.Data.Config as Conf
import qualified Graphics.Gloss.Interface.IO.Game as G
import qualified NeuralNetwork.Network as NN
import qualified Data.Vector as V

-- | default car sensor positions
carSensorPoints :: (MonadReader Conf.Config m) => m [Point]
carSensorPoints = do
    config <- ask
    let carSensorCount = Conf.carSensorCount config
        diffTheta = (degToRad 90) / fromIntegral (carSensorCount - 1)
        l = take carSensorCount $ iterate (+ diffTheta) $ degToRad (-45)
    mapM getSensorOfAngle l

-- | the `Car`'s status
data Status = Alive | Dead Int deriving (Eq, Show, Read)

-- | car
data Car = Car
    {
      -- | the "intelligence"
      network :: NN.Network
      -- | where the car is
    , position :: Point
      -- | where the car is facing, unit is radians
    , orientation :: Float
      -- | the car's status
    , status :: Status
    }

instance Show Car where
    show (Car nn _ _ _) = show nn

instance Read Car where
    readsPrec _ s = case reads s of
        [(nn, s')] -> [(Car nn def 0 (Dead 0), s')]
        v           -> error $ show v

instance CanDraw Car where
    draw (Car _ p o _) = do
        config <- ask
        let (w, h) = Conf.carSize config
            p1 = ((-w / 2), (-h / 2))
            p2 = ((w / 2), (-h / 2))
            p3 = ((w / 2), (h / 2))
            p4 = ((-w / 2), (h / 2))
            body = G.polygon [p1, p2, p3, p4]
        eyes <- sequence [draw $ Point p2, draw $ Point p3]
        let eyes' = G.color G.red $ G.pictures eyes
        pure $ G.translate x y $ G.rotate oInDegree $ G.pictures [body, eyes']
        where
        (x, y) = getTuple p
        oInDegree = -(radToDeg o)

instance CanReproduce Car where
    reproduce c1 c2 = do
        newNetwork <- reproduce (network c1) (network c2)
        pure $ c1{network = newNetwork}
    mutate f c = do
        newNetwork <- mutate f $ network c
        pure $ c{network = newNetwork}

-- | helper function, given a point and angle(in radians) output where
--   the point would end up after rotation. center is origin(0, 0).
rotate :: Float -> Point -> Point
rotate r p = Point (d * cos theta, d * sin theta)
    where
    (x, y) = getTuple p
    d = distance (Point (0, 0)) p
    theta = r + atan2 y x

-- | get four corner points of the car
getFourPoints :: (MonadReader Conf.Config m)
              => Car
              -> m (Point, Point, Point, Point)
getFourPoints (Car _ p o _) = do
    config <- ask
    let (w, h) = Conf.carSize config
        p1 = Point ((-w / 2), (-h / 2))
        p2 = Point ((w / 2), (-h / 2))
        p3 = Point ((w / 2), (h / 2))
        p4 = Point ((-w / 2), (h / 2))
        newP1 = p .+. rotate o p1
        newP2 = p .+. rotate o p2
        newP3 = p .+. rotate o p3
        newP4 = p .+. rotate o p4
    pure (newP1, newP2, newP3, newP4)

-- | get a sensor position of a particular angle
getSensorOfAngle :: (MonadReader Conf.Config m) => Float -> m Point
getSensorOfAngle angle = do
    config <- ask
    let (w, h) = Conf.carSize config
        lb = ((-w / 2), (-h / 2)) -- left bottom
        rb = ((w / 2), (-h / 2)) -- right bottom
        rt = ((w / 2), (h / 2)) -- right top
        lt = ((-w / 2), (h / 2)) -- left top
        ips = -- intersecting points
            [ intersectLineLine (0, 0) (cos angle, sin angle) lt rt
            , intersectLineLine (0, 0) (cos angle, sin angle) rt rb
            , intersectLineLine (0, 0) (cos angle, sin angle) rb lb
            ]
    pure
        -- choose the minimum of all distances
        $ minimumBy (compare `on` distance (Point (0, 0)))
        $ map Point            -- wrap them with Point first
        $ filter ((> 0) . fst) -- only keep points that has x > 0
        $ map fromJust         -- get rid of the Maybe wrapper
        $ filter isJust ips    -- filter out all Nothing

-- | get all sensors
getSensorPoints :: (MonadReader Conf.Config m) => Car -> m [Point]
getSensorPoints c = map (.+. p) <$> map (rotate o) <$> carSensorPoints
    where
    p = position c
    o = orientation c

-- | initialized car
initCar :: (MonadReader Conf.Config m, MonadIO m) => m Car
initCar = do
    config <- ask
    let carSensorCount = Conf.carSensorCount config
    nn <- liftIO $ NN.generate [carSensorCount, 4, 3, 2]
    pure $ Car nn def 0 (Dead 0)

-- | start the car
start :: Point -> Float -> Car -> Car
start p o c = c{position = p, orientation = o, status = Alive}

-- | tell a car that it is killed, and when it is killed
kill :: Int -> Car -> Car
kill t c = case status c of
    Dead _ -> c
    Alive -> c{status = Dead t}

-- | each step after starting the car
step :: (MonadReader Conf.Config m) => V.Vector Double -> Car -> m Car
step _ c@(Car _ _ _ (Dead _)) = pure c
step iv (Car nn p o Alive) = do
    config <- ask
    let carMaximumSpeed = Conf.carMaximumSpeed config
        carMaximumTurning = Conf.carMaximumTurning config
        carSensorCapbility = Conf.carSensorCapbility config
        iv' = V.map ((subtract 1) . (* 2) . (/ rtf2 carSensorCapbility)) iv
    case V.toList $ NN.output iv' nn of
        [enginePower, turning] ->
            let speed = rtf enginePower * carMaximumSpeed
                newT = (rtf turning * 2 - 1) * carMaximumTurning
                newX = x + speed * cos o
                newY = y + speed * sin o
                newO = normalizeAngle $ o + newT in
            pure $ Car nn (Point (newX, newY)) newO Alive
        _ -> error "won't happen"
    where
    (x, y) = getTuple p
    rtf = realToFrac
    rtf2 = realToFrac
