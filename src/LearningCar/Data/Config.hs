{- |
    this module defines config data type
-}
module LearningCar.Data.Config
    ( Config(..)
    , getConfig
    ) where
import Data.Default (Default, def)
import Control.Applicative (empty)
import System.Directory (doesFileExist)
import Graphics.Gloss.Geometry.Angle (degToRad, radToDeg)
import qualified Data.Map as Map
import qualified Data.Parser as P

-- | config data type
data Config = Config
    {
      -- environment
      screenSize :: (Int, Int)
      -- game general setting
      -- | the window's name
    , title :: String

      -- game world setting
      -- | how many cars are in each generation
    , carCount :: Int
      -- | maximum tick for each generation
    , maxTick :: Int

      -- car setting
      -- | car's size
    , carSize :: (Float, Float)
      -- | how many sensor does a car has
    , carSensorCount :: Int
      -- | car's sensor's capbility
    , carSensorCapbility :: Float
      -- | car's mutation rate
    , carMutationRate :: Float
      -- | car's max speed
    , carMaximumSpeed :: Float
      -- | car's max turning angle
    , carMaximumTurning :: Float
    }

instance Default Config where
    def = Config (0, 0) "hello world" 20 1000 (20, 10) 5 50 0.1 2 5

instance Show Config where
    show c = unlines
        [ "title = " ++ title c
        , "carCount = " ++ show (carCount c)
        , "maxTick = " ++ show (maxTick c)
        , "carSize = " ++ show (carSize c)
        , "carSensorCount = " ++ show (carSensorCount c)
        , "carSensorCapbility = " ++ show (carSensorCapbility c)
        , "carMutationRate = " ++ show (carMutationRate c)
        , "carMaximumSpeed = " ++ show (carMaximumSpeed c)
        , "carMaximumTurning = " ++ show (radToDeg $ carMaximumTurning c)
        ]

-- | ignore all spaces
ignoreSpaces :: P.Parser ()
ignoreSpaces = P.zeroOrMore P.space *> pure ()

grabAll :: P.Parser String
grabAll = P.oneOrMore $ P.match $ const $ True

-- | parse number
parseReadable :: (Read a) => P.Parser a
parseReadable = (ignoreSpaces *> grabAll)>>= f
    where
    f v = case reads v of
        ((v', []):_) -> pure v'
        _ -> empty

-- | parse tuple
parseTuple :: P.Parser a -> P.Parser b -> P.Parser (a, b)
parseTuple pa pb = do
    _ <- ignoreSpaces *> P.match (== '(')
    a <- ignoreSpaces *> pa
    _ <- ignoreSpaces *> P.match (== ',')
    b <- ignoreSpaces *> pb
    _ <- ignoreSpaces *> P.match (== ')')
    pure (a, b)

-- | parse one line of the config
parseConfigLine :: String -> Maybe (String, String)
parseConfigLine s = fst <$> P.runParser p s
    where
    p = do
        s1 <- ignoreSpaces *> P.oneOrMore P.alphanumeric
        _ <- ignoreSpaces *> P.match (== '=')
        s2 <- ignoreSpaces *> (P.oneOrMore $ P.match $ const True)
        pure (s1, s2)

-- | parse the config file to map data structure
parseConfig :: String -> Map.Map String String
parseConfig = f Map.empty . lines
    where
    f m [] = m
    f m (x:xs) = case parseConfigLine x of
        Nothing -> f m xs
        Just (k, v) -> f (Map.insert k v m) xs

-- | transform established map to Config data structure
mapToConfig :: Map.Map String String -> Config
mapToConfig m = f def
    where
    pt = parseTuple parseReadable parseReadable
    oneMember dv s p
        = case (fst <$> (Map.lookup s m >>= P.runParser p)) of
            Nothing -> dv
            Just v -> v
    f c = c
        { title = oneMember (title c) "title" grabAll
        , carCount = oneMember (carCount c) "carCount" parseReadable
        , maxTick = oneMember (maxTick c) "maxTick" parseReadable
        , carSize = oneMember (carSize c) "carSize" pt
        , carSensorCount = oneMember (carSensorCount c) "carSensorCount" parseReadable
        , carSensorCapbility = oneMember (carSensorCapbility c) "carSensorCapbility" parseReadable
        , carMutationRate = oneMember (carMutationRate c) "carMutationRate" parseReadable
        , carMaximumSpeed = oneMember (carMaximumSpeed c) "carMaximumSpeed" parseReadable
        , carMaximumTurning = degToRad $ oneMember (carMaximumTurning c) "carMaximumTurning" parseReadable
        }

-- | get config data
getConfig :: IO Config
getConfig = do
    b <- doesFileExist "config"
    content <- case b of
        True -> readFile "config"
        _    -> pure ""
    pure $ mapToConfig (parseConfig content)
