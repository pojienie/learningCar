{-# LANGUAGE FlexibleContexts #-}
{- |
    this module defines `CanDraw` class
-}
module LearningCar.Data.CanDraw
    ( CanDraw(..)
    ) where

import Control.Monad.Reader (MonadReader(..))
import qualified LearningCar.Data.Config as Conf
import qualified Graphics.Gloss as G

-- | any thing that is can draw will have this class
class CanDraw a where
    draw :: (MonadReader Conf.Config m) => a -> m G.Picture
