{-# LANGUAGE FlexibleContexts #-}
{- |
    Deep learning car library
-}
module LearningCar.Data.GameWorld
    ( GameWorld
    , Status(..)
    , addWall
    , addProgressPoint
    , initWorld
    , start
    , step
    , placeStartingPoint
    , placeGoalPoint
    , getInfoString
    ) where

import LearningCar.Data.CanDraw (CanDraw, draw)
import LearningCar.Data.Point (Point(..), getTuple, distance)
import LearningCar.Data.Wall (Wall(..), point1, point2)
import Data.Maybe (isJust)
import Data.List (sortBy, maximumBy, init)
import Data.Function (on)
import Data.Genetic (reproduce, mutate)
import Control.Monad (replicateM, zipWithM)
import Control.Monad.Reader (MonadReader(..), MonadIO(..))
import qualified LearningCar.Data.Config as Conf
import qualified Graphics.Gloss.Geometry.Line as L
import qualified Data.Vector as V
import qualified LearningCar.Data.Car as C
import qualified Graphics.Gloss.Interface.IO.Game as G

-- | the `GameWorld`'s status
data Status = Started Int | Idle deriving (Eq, Show, Read)

-- | a game world has a `GameMap`
data GameWorld = GameWorld
    {
      -- | walls
      walls :: [Wall]
      -- | used for score calculation, first element is starting point
      --   last element is goal point
    , progressLine :: [Point]
      -- | which generation it is
    , generation :: Int
      -- | the cars' goals
    , cars :: [C.Car]
      -- | the current status
    , _status :: Status
    }

instance Show GameWorld where
    show (GameWorld w p g c _) = show (w, p, g, c)

instance Read GameWorld where
    readsPrec _ s = case reads s of
        [((w, p, g, c), s')] -> [(GameWorld w p g c Idle, s')]
        _                    -> []

instance CanDraw GameWorld where
    draw (GameWorld ws pl _ cs _) = do
        config <- ask
        let maxTick = Conf.maxTick config
            bestCar = maximumBy (compare `on` score maxTick pl) cs
        ws' <- G.pictures <$> mapM draw ws
        cs' <- G.pictures <$> mapM draw cs
        bestCar' <- draw bestCar
        let bestCarsPL = truncateToCarPoint pl (C.position bestCar)
        pure $ G.pictures
            [ ws'
            , cs'
            , G.color G.green $ G.Line $ map getTuple pl -- progress line
            , G.color G.blue bestCar'
            , G.color G.red $ G.Line $ map getTuple bestCarsPL
            ]

-- | create a initialized world
initWorld :: (MonadReader Conf.Config m, MonadIO m) => m GameWorld
initWorld = do
    config <- ask
    let carCount = Conf.carCount config
    cs <- replicateM carCount C.initCar
    pure $ GameWorld [] [s, g] 0 cs Idle
    where
    s = Point (0, 0)
    g = Point (100, 100)

-- | change starting point
placeStartingPoint :: Point -> GameWorld -> GameWorld
placeStartingPoint p g = g{progressLine = newPL}
    where
    newPL = p : (drop 1 $ progressLine g)

-- | change goal point
placeGoalPoint :: Point -> GameWorld -> GameWorld
placeGoalPoint p g = g{progressLine = newPL}
    where
    newPL = (init $ progressLine g) ++ [p]

-- | add `Wall` to `GameWorld`
addWall :: Wall -> GameWorld -> GameWorld
addWall w g = g{walls = w : walls g}

-- | add progress point to `GameWorld`
addProgressPoint :: Point -> GameWorld -> GameWorld
addProgressPoint p g = g{progressLine = newPL}
    where
    goal = last $ progressLine g
    newPL = (init $ progressLine g) ++ [p, goal]

-- | start the game
start :: GameWorld -> GameWorld
start (GameWorld ws pl gen cs _) = GameWorld ws pl (gen + 1) newCs $ Started 0
    where
    newCs = map (C.start (head pl) 0) cs

-- | each step after starting the game
step :: (MonadReader Conf.Config m, MonadIO m)
        => GameWorld
        -> m GameWorld
step w@(GameWorld _ _ _ _ Idle) = pure w
step (GameWorld ws pl gen cs (Started tick)) = do
    config <- ask
    let carCount = Conf.carCount config
        maxTick = Conf.maxTick config
        carMutationRate = Conf.carMutationRate config
        getNewGen = allCarsDead cs || tick > maxTick
    if getNewGen then do
        [f, m] <- pure $ drop (carCount - 2) $ sortBy (compare `on` (score maxTick pl)) cs
        newCs <- liftIO $ replicateM carCount (reproduce f m)
        newCs2 <- liftIO $ mapM (mutate carMutationRate) newCs
        pure $ start $ GameWorld ws pl gen newCs2 Idle
    else do
        new <- newCars
        pure $ GameWorld ws pl gen new (Started $ tick + 1)
    where
    newCar c = case C.status c of
        C.Dead _ -> pure c
        C.Alive -> do
            ccw <- collideCarWalls c ws
            if ccw then pure $ C.kill tick c else pure c
    newCars = do
        civ <- mapM iv cs
        l <- zipWithM C.step civ cs
        mapM newCar l
    iv c = V.fromList <$> map rtf <$> getCarSensorInput c ws
    rtf = realToFrac

-- | check if all cars are dead
allCarsDead :: [C.Car] -> Bool
allCarsDead = foldl (&&) True . map (isDead . C.status)
    where
    isDead (C.Dead _) = True
    isDead _ = False

-- | get sensor input against a wall, ignoring all other walls
getSensorInputAgainstAWall :: (MonadReader Conf.Config m)
                           => Wall
                           -> Point
                           -> Point
                           -> m Float
getSensorInputAgainstAWall w cp sp = do
    config <- ask
    let carSensorCapbility = Conf.carSensorCapbility config
    case L.intersectSegLine wp1 wp2 cp' sp' of
        Nothing -> pure carSensorCapbility
        Just v -> pure $ minimum [distance sp (Point v), carSensorCapbility]
    where
    wp1 = getTuple $ point1 w
    wp2 = getTuple $ point2 w
    cp' = getTuple cp
    sp' = getTuple sp

-- | get a sensor's input
getSensorInput :: (MonadReader Conf.Config m)
               => [Wall]
               -> Point
               -> Point
               -> m Float
getSensorInput [] _ _ = ask >>= pure . Conf.carSensorCapbility
getSensorInput ws cp sp
    = minimum <$> mapM (\w -> getSensorInputAgainstAWall w cp sp) ws

-- | get a car's sensors' input
getCarSensorInput :: (MonadReader Conf.Config m) => C.Car -> [Wall] -> m [Float]
getCarSensorInput c ws = C.getSensorPoints c >>= mapM (getSensorInput ws cp)
    where
    -- car's position
    cp = C.position c

-- | insert all points where the car casts a shadow on
--
-- @
--              c = car
--              |
--              |
--  Line -------s-----  s = shadow
-- @
insertCarPoint :: [Point] -> Point -> [Point]
insertCarPoint [] _ = []
insertCarPoint [a] _ = [a]
insertCarPoint (p1:p2:pl) c
        | 0 <= param && param <= 1 = p1 : Point s : insertCarPoint (p2:pl) c
        | otherwise = p1 : insertCarPoint (p2:pl) c
    where
    tp1 = getTuple p1
    tp2 = getTuple p2
    tc = getTuple c
    param = L.closestPointOnLineParam tp1 tp2 tc
    s = L.closestPointOnLine tp1 tp2 tc

-- | return the index of the point which the second point is cloest to
closestPointIndex :: [Point] -> Point -> Int
closestPointIndex [] _ = undefined
closestPointIndex (p':pl') c = f 0 1 (distance p' c) pl'
    where
    -- i = closest index, ci = current index, cd = cloest distance
    f i _ _ [] = i
    f i ci cd (p:pl)
        | distance p c < cd = f ci (ci + 1) (distance p c) pl
        | otherwise = f i (ci + 1) cd pl

-- | this output the progress line up to where the car is
truncateToCarPoint :: [Point] -> Point -> [Point]
truncateToCarPoint pl p = take (closestPointIndex pl' p + 1) pl'
    where
    pl' = insertCarPoint pl p

-- | calcuate the progress of a given car
progressCar :: [Point] -> C.Car -> Float
progressCar pl c = carLength / wholeLength
    where
    carP = C.position c
    wholeLength = sum $ zipWith distance pl $ tail pl
    carLength = sum $ zipWith distance <$> id <*> tail $ truncateToCarPoint pl carP

-- | calculate score of a car
score :: Int -> [Point] -> C.Car -> Float
score maxTick pl c = progressScore * 3 + aliveTimeScore * 0.1
    where
    progressScore = progressCar pl c
    aliveTimeScore = case C.status c of
        C.Alive -> 1
        C.Dead t -> fromIntegral t / fromIntegral maxTick

-- | given a car and a bunch of walls, decides if they collide any wall or not
collideCarWalls :: (MonadReader Conf.Config m) => C.Car -> [Wall] -> m Bool
collideCarWalls c w = foldl (||) False <$> mapM (collideCarWall c) w

-- | given a car and a wall, decides if they collide or not
collideCarWall :: (MonadReader Conf.Config m) => C.Car -> Wall -> m Bool
collideCarWall c w = do
    -- car point x
    (cp1, cp2, cp3, cp4) <- ap getTuple <$> C.getFourPoints c
    pure $ foldl (||) False
        [
          isJust $ L.intersectSegSeg wp1 wp2 cp1 cp2
        , isJust $ L.intersectSegSeg wp1 wp2 cp2 cp3
        , isJust $ L.intersectSegSeg wp1 wp2 cp3 cp4
        , isJust $ L.intersectSegSeg wp1 wp2 cp1 cp4
        ]
    where
    ap f (a, b, c', d) = (f a, f b, f c', f d)
    -- wall point x
    wp1 = getTuple $ point1 w
    wp2 = getTuple $ point2 w

-- | get information string of this game world
getInfoString :: (MonadReader Conf.Config m) => GameWorld -> m [String]
getInfoString w = do
    config <- ask
    let maxTick = Conf.maxTick config
        pl = progressLine w
        cs = cars w
        bestCar = maximumBy (compare `on` score maxTick pl) cs
        progress = progressCar pl bestCar
    pure
      [ "generation: " ++ show (generation w)
      , "progress: " ++ show (progress)
      ]
