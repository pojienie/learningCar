{- |
    this module defines `Wall` datatype
-}
module LearningCar.Data.Wall
    ( Wall(..)
    ) where

import LearningCar.Data.Point (Point(..))
import LearningCar.Data.CanDraw (CanDraw, draw)
import qualified Graphics.Gloss as G

-- | a wall has two points
data Wall = Wall
    {
      -- | one of the points of Wall
      point1 :: Point
      -- | the other point of Wall
    , point2 :: Point
    } deriving (Show, Read)

instance CanDraw Wall where
    draw (Wall p1' p2') = pure $ G.line [p1, p2]
        where
        p1 = getTuple p1'
        p2 = getTuple p2'
