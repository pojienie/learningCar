{- |
    Deep learning car library
-}
module LearningCar
    ( main
    ) where

import Data.Default (def)
import LearningCar.Data.CanDraw (draw)
import LearningCar.Data.Point (Point(..))
import LearningCar.Data.Wall (Wall(..))
import System.Directory (doesFileExist)
import Control.Monad.Reader (Reader, runReader, ReaderT, runReaderT, ask, lift)
import Graphics.Gloss.Interface.Environment(getScreenSize)
import qualified LearningCar.Data.Config as Conf
import qualified Graphics.Gloss.Interface.IO.Game as G
import qualified LearningCar.Data.GameWorld as W

-- | GameData stores everything this game needs
data GameData = GameData
    {
      -- | where you clicked last time
      lastClick :: Maybe Point
      -- | current mouse point
    , currentMouse :: Point
      -- | game world
    , gameWorld :: W.GameWorld
      -- | game's speed
    , gameSpeed :: Int
    }

-- | draw information of game world
drawInfo :: GameData -> Reader Conf.Config G.Picture
drawInfo gd = do
    config <- ask
    let (w', h') = Conf.screenSize config
        w = fromIntegral w'
        h = fromIntegral h'
        speedS = "speed: " ++ show (gameSpeed gd)
        world = gameWorld gd
    infoString <- (speedS :) <$> W.getInfoString world
    pure
      $ G.translate (-w / 2) (h / 2)
      $ G.scale 0.25 0.25
      $ G.pictures
      $ zipWith (G.translate 0) yList
      $ map G.text infoString
    where
    characterHeight = 100 -- this is calculated by trial and error
    yList = iterate (subtract characterHeight) (-characterHeight)

-- | draw function
glossDraw :: GameData -> Reader Conf.Config G.Picture
glossDraw gd@(GameData Nothing _ w _) = do
    info <- drawInfo gd
    w' <- draw w
    pure $ G.pictures [info, w']
glossDraw gd@(GameData (Just l) c w _) = do
    config <- ask
    info <- drawInfo gd
    w' <- runReaderT (draw w) config
    pure $ G.pictures [info, G.line [getTuple l, getTuple c], w']

-- | save the game's data
save :: Conf.Config -> W.GameWorld -> IO ()
save c gw = do
    writeFile ("gameSave") $ show gw
    writeFile ("config") $ show c

-- | load the game's data
load :: IO (Maybe W.GameWorld)
load = do
    b <- doesFileExist ("gameSave")
    if b then do
        content <- readFile ("gameSave")
        case reads content of
            [(v, [])] -> pure $ Just v
            _ -> pure $ Nothing
    else
        pure Nothing

-- | input handler
glossInput :: G.Event -> GameData -> ReaderT Conf.Config IO GameData
-- clicking left mouse button
glossInput (G.EventKey (G.MouseButton G.LeftButton) G.Down _ p) gd
    = pure $ GameData (Just $ Point p) (Point p) w (gameSpeed gd)
    where
    w = gameWorld gd
-- moving mouse
glossInput (G.EventMotion p) gd = pure gd{currentMouse = Point p}
-- releasing left mouse button
glossInput (G.EventKey (G.MouseButton G.LeftButton) G.Up _ p) gd
    = pure $ GameData Nothing newPoint newWorld (gameSpeed gd)
    where
    newPoint = Point p
    newWorld = case lastClick gd of
        Just p' -> W.addWall (Wall p' newPoint) $ gameWorld gd
        _ -> gameWorld gd
-- press F1 to save
glossInput (G.EventKey (G.SpecialKey G.KeyF1) G.Down _ _) gd = do
    config <- ask
    lift $ save config (gameWorld gd)
    pure gd
-- press F2 to load
glossInput (G.EventKey (G.SpecialKey G.KeyF2) G.Down _ _) gd = do
    w <- lift load
    case w of
        Just v -> pure gd{gameWorld = v}
        Nothing -> lift (putStrLn "load failed") >> pure gd
-- press + to increase game speed
glossInput (G.EventKey (G.Char '+') G.Down _ _) gd
    | gameSpeed gd == 8 = pure gd
    | otherwise = pure gd{gameSpeed = gameSpeed gd * 2}
-- press - to decrease game speed
glossInput (G.EventKey (G.Char '-') G.Down _ _) gd
    | gameSpeed gd == 1 = pure gd
    | otherwise = pure gd{gameSpeed = gameSpeed gd `div` 2}
-- press 1 to place starting point
glossInput (G.EventKey (G.Char '1') G.Down _ p) gd
    = pure $ gd{gameWorld = W.placeStartingPoint (Point p) $ gameWorld gd}
-- press 2 to place goal point
glossInput (G.EventKey (G.Char '2') G.Down _ p) gd
    = pure $ gd{gameWorld = W.placeGoalPoint (Point p) $ gameWorld gd}
-- press 3 to place progress point
glossInput (G.EventKey (G.Char '3') G.Down _ p) gd
    = pure $ gd{gameWorld = W.addProgressPoint (Point p) $ gameWorld gd}
-- press enter to start the game
glossInput (G.EventKey (G.SpecialKey G.KeyEnter) G.Down _ _) gd
    = pure $ gd{gameWorld = W.start $ gameWorld gd}
glossInput _ w = pure w

-- | helper function, this is monadic iterate but with action encapsulated
--   within the list
iterateM :: (Monad m) => (a -> m a) -> m a -> [m a]
iterateM f v = v : iterateM f (v >>= f)

-- | step
step :: Float -> GameData -> ReaderT Conf.Config IO GameData
step _ gd = do
    nw <- iterateM W.step (pure gw) !! sp
    pure $ gd{gameWorld = nw}
    where
    gw = gameWorld gd
    sp = gameSpeed gd

-- | entry point of LearningCar lib
main :: IO ()
main = do
    config <- Conf.getConfig
    world <- runReaderT W.initWorld config
    screenSize <- getScreenSize
    let config' = config{Conf.screenSize = screenSize}
        gd = GameData Nothing def world 1
        title = Conf.title config'
        winMode = G.InWindow title (100, 100) (1000, 1000)
    G.playIO
         winMode                                       -- window mode
         G.white                                       -- background color
         60                                            -- fps
         gd                                            -- world
         (\g -> pure $ runReader (glossDraw g) config')-- draw
         (\e g -> runReaderT (glossInput e g) config') -- input
         (\f g -> runReaderT (step f g) config')       -- step
