# LearningCar

this is inspired by: https://arztsamuel.github.io/en/projects/unity/deepCars/deepCars.html

# how it works

At the very start, 20 cars are created, each car has its own neural network(randomly generated) and five sensors that detect distances of the walls. Now cars will start. Cars that collides a wall dies immediately. After certain time or all car dies, the generation ends.

When a generation ends, two best scored cars are selected. Those two cars will reproduce 20  using genetic algorithm. All weights and biases in the network are randomly chosen from one of the best cars. Then mutation is applied. Default mutation rate is 10%, meaning that for each weight and bias, there is 10% chances it is mutated. If a weight or bias is mutated, a number is added to the current weight or bias. The number is produced using normal distribution.

After the above procedure is done, a new generation begins. And all car will try to maneuver through the course. And the above procedure repeats.

# how to build

    git clone http://github.com/PoJieNie/Learning-car
    stack build

# how to play

For now, the game has no in-game manual. Here is all the control:

drag your left mouse key to draw wall.

press '1' to place starting point in your current mouse cursor position.

press '2' to place goal point in your current mouse cursor position.

press '3' to place "progression" point in your current mouse cursor position.

press 'F1' to save config file and game state

press 'F2' to load game state

press 'enter' to begin.
