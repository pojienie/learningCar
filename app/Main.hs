{- |
    entry module
-}
module Main where

import qualified LearningCar as DLC

-- | entry point
main :: IO ()
main = DLC.main
